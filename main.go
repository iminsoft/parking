package main

import (
	"log"

	"bitbucket.org/iminsoft/parking/event"
	"bitbucket.org/iminsoft/parking/handler"
	micro "github.com/micro/go-micro"
)

func main() {

	service := micro.NewService(
		micro.Name("im.srv.parking"),
	)

	service.Init()
	handler.Register(service.Server())
	event.ListenCreatedEmployees()

	if err := service.Run(); err != nil {
		//db.Close()
		log.Fatal(err)
	}

}
