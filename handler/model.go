package handler

import (
	"time"
)

type (
	CreateRequest struct {
		Gid     string `json:"gid"`
		Parking string `json:"parking"`
		Type    string `json:"type"`
		From    time.Time
		To      time.Time
	}

	EmployeeRequest struct {
		Gid string `json:"gid"`
	}

	InfoRecord struct {
		Parking       string  `json:"parking"`
		RequestedType string  `json:"requested"`
		AssignedType  string  `json:"assigned, ommitempty"`
		From          string  `json:"from"`
		To            string  `json:"to"`
		Position      uint    `json:"ranking, ommitempty"`
		User          string  `json:"user"`
		Points        float64 `json:"points"`
		SubscribedAt  string  `json:"subscribed_at"`
		ProcessedAt   string  `json:"processed_at, ommitempty"`
	}

	InfoResponse struct {
		Current InfoRecord `json:"current"`
	}

	HistoryResponse struct {
		Subscriber SubscriberRecord
		History    []InfoRecord
	}

	SubscriberRecord struct {
		Name    string
		HiredAt string
	}

	ParkingRecord struct {
		Name string
	}

	ParkingRequest struct {
		Name string
	}

	ParkingSubscribersResponse struct {
		Subscriptions []InfoRecord
	}

	ParkingListResponse struct {
		Parking []ParkingRecord
	}

	DateRequest struct {
		Date string
	}

	EmptyRequest struct {
	}

	EmptyResponse struct {
	}
)
