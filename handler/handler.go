package handler

import (
	"time"

	"fmt"

	"bitbucket.org/iminsoft/parking/service"
	"bitbucket.org/iminsoft/parking/service/domain"
	"github.com/micro/go-micro/server"
	c "golang.org/x/net/context"
)

var (
	parking service.Service
)

type (
	Subscription struct{}
	Parking      struct{}
	User         struct{}
)

func init() {
	parking = service.NewService(
		service.Listen(service.OnInit, func(p *domain.Parking) {
			fmt.Printf("%s parking initialized, there is already %d subscriptions for <%s %s> period\n",
				p.ID,
				len(p.Period.Subscriptions),
				p.Period.From.Format("2006-01-02"),
				p.Period.To.Format("2006-01-02"))
		}),
		service.Listen(service.OnPeriodOpen, func(p *domain.Parking) {
			fmt.Printf("Parking %s period#%d (%s : %s) has been opened\n",
				p.ID,
				p.Period.ID,
				p.Period.From.Format("2006-01-02"),
				p.Period.To.Format("2006-01-02"))
		}),
		service.Listen(service.OnPeriodClosed, func(p *domain.Parking) {
			fmt.Printf("Parking %s period#%d (%s : %s) has been closed, with %d subscribers\n",
				p.ID,
				p.Period.ID,
				p.Period.From.Format("2006-01-02"),
				p.Period.To.Format("2006-01-02"),
				len(p.Period.Subscriptions))

		}),
	)
}

func (h *Subscription) Date(ctx c.Context, req *DateRequest, res *EmptyResponse) error {
	var err error
	d, err := time.Parse("2006-01-02", req.Date)
	if err != nil {
		return err
	}
	parking.HackDate(d)

	return err
}

func (h *Subscription) Create(ctx c.Context, req *CreateRequest, res *EmptyResponse) error {

	return parking.Subscribe(req.Gid, req.Parking, req.Type)
}

func (h *Subscription) Status(ctx c.Context, req *EmployeeRequest, res *InfoResponse) error {

	s, err := parking.Score(req.Gid)
	if err != nil {
		return err
	}

	res.Current = toInfoRecord(s)

	return nil
}

func (h *Subscription) Cancel(ctx c.Context, req *EmployeeRequest, res *EmptyResponse) error {
	return parking.Unsubscribe(req.Gid)
}

func (h *Subscription) History(ctx c.Context, req *EmployeeRequest, res *HistoryResponse) error {
	u, err := parking.User(req.Gid)
	if err != nil {
		return err
	}

	res.Subscriber.Name = u.ID
	res.Subscriber.HiredAt = u.HiredAt.Format("2006-01-02")

	for _, ds := range u.Subscriptions {
		res.History = append(res.History, toInfoRecord(ds))
	}

	return nil
}

func (h *Parking) List(ctx c.Context, req *EmptyRequest, res *ParkingListResponse) error {
	for _, p := range parking.List() {
		res.Parking = append(res.Parking, toParkingRecord(p))
	}

	return nil
}

func (h *Parking) Subscriptions(ctx c.Context, req *ParkingRequest, res *ParkingSubscribersResponse) error {
	sl, err := parking.Ranking(req.Name)
	if err != nil {
		return err
	}

	for _, ds := range sl {
		res.Subscriptions = append(res.Subscriptions, toInfoRecord(ds))
	}

	return nil

}

func Register(s server.Server) {

	s.Handle(s.NewHandler(&Subscription{}))
	s.Handle(s.NewHandler(&Parking{}))
	s.Handle(s.NewHandler(&User{}))
}

func toParkingRecord(dp domain.Parking) ParkingRecord {
	return ParkingRecord{
		Name: dp.ID,
	}
}

func toInfoRecord(ds domain.Subscription) InfoRecord {
	return InfoRecord{
		Parking:       ds.ParkingID,
		User:          ds.UserID,
		From:          ds.From.Format("2006-01-02"),
		To:            ds.To.Format("2006-01-02"),
		RequestedType: ds.RequestedType,
		AssignedType:  ds.AssignedType,
		Position:      ds.Position,
		Points:        ds.Points,
		SubscribedAt:  ds.SubscribedAt.Format("2006-01-02 15:04"),
		ProcessedAt:   ds.ProcessedAt.Format("2006-01-02"),
	}
}
