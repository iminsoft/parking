package event

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/micro/go-micro/broker"

	"time"
)

func init() {

	if err := broker.Init(); err != nil {
		log.Fatalf("Broker Init error: %v", err)
	}

	if err := broker.Connect(); err != nil {
		log.Fatalf("Broker Connect error: %v", err)
	}
}

func ListenCreatedEmployees() {
	listen("employee.created", func(in []byte) error {
		e := map[string]string{}
		err := json.Unmarshal(in, &e)
		if err != nil {
			return err
		}

		hiredAt, err := time.Parse(time.RFC3339, e["created_at"])
		if err != nil {
			return err
		}
		fmt.Println(hiredAt)
		//err = parking.Service().Person().Create(e["gid"], hiredAt)
		//if err != nil {
		//	log.Printf(err.Error())
		//}

		return nil
	})
}

func listen(topic string, f func([]byte) error) {
	_, err := broker.Subscribe(topic, func(p broker.Publication) error {
		return f(p.Message().Body)
	})

	if err != nil {
		fmt.Println(err)
	}
}
