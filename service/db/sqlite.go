package db

import (
	"time"

	"sync"

	"fmt"

	. "bitbucket.org/iminsoft/parking/service/domain"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

var (
	dbName = "parking.db"
	db     *gorm.DB
	mutex  = &sync.Mutex{}
)

func init() {
	var err error

	db, err = gorm.Open("sqlite3", "./"+dbName)
	if err != nil {
		panic(err)
	}

	db.Exec("PRAGMA foreign_keys = ON")
	//db.LogMode(true)

	db.AutoMigrate(&Parking{}, &Period{}, &User{}, &Subscription{})
}

func StoreParking(p *Parking) error {
	mutex.Lock()
	db.Save(&p.Period)
	for i := range p.Period.Subscriptions {
		s := &p.Period.Subscriptions[i]
		s.PeriodID = p.Period.ID
		s.Processed = true
		s.ProcessedAt = time.Now()
		db.Set("gorm:save_associations", false).Model(s).Save(s)
	}

	mutex.Unlock()

	return db.Error
}

func FindPerson(name string) (User, error) {
	var e User
	db.Find(&e, "id = ?", name)

	if db.RecordNotFound() {
		return e, fmt.Errorf("user %s not found", name)
	}

	return e, nil
}

func FindPersonWithScore(id string) (User, error) {
	var e User

	err := db.Find(&e, "id = ?", id).Error
	if err == gorm.ErrRecordNotFound {
		return e, fmt.Errorf("user %s not found", id)
	}

	db.Find(&e.Subscriptions, "user_id = ? AND processed = ?", e.ID, 1)

	return e, nil
}

// Load all active parking with current Period (t) and all Period Subscribers
func LoadAllParking(t time.Time) ([]*Parking, error) {
	var sp []*Parking

	db.Find(&sp)
	for _, p := range sp {
		t = time.Now().AddDate(0, p.MonthsSwitch(), 0)
		db.First(&p.Period, `parking_id = ? AND ? BETWEEN "from" AND "to"`, p.ID, t.Format("2006-01-02"))

		if db.RecordNotFound() {
			break
		}

		db.Find(&p.Period.Subscriptions, "period_id = ?", p.Period.ID)
	}

	return sp, nil
}
