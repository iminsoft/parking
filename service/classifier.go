package service

import (
	"fmt"
	"sort"

	"bitbucket.org/iminsoft/parking/service/domain"
)

type (
	csfr struct{}
)

func (c csfr) Classify(pg *domain.Parking, f func(domain.Subscription)) error {
	var points []float64
	var values = make(map[float64][]*domain.Subscription)

	limits := pg.Groups()
	groups := pg.Groups()
	mapping := pg.Order()

	if len(pg.Period.Subscriptions) == 0 {
		return fmt.Errorf("no subscribers in parking %s, there is nothing to classify", pg.ID)
	}

	for i, _ := range pg.Period.Subscriptions {
		s := &pg.Period.Subscriptions[i]
		c.clear(s)
		if _, ok := values[s.Points]; !ok {
			points = append(points, s.Points)
		}
		values[s.Points] = append(values[s.Points], s)
	}

	c.sortPoints(points)
	for _, p := range points {
		for _, s := range values[p] {
			called := false
			// check if requested group is defined in parking
			m, ok := mapping[s.RequestedType]
			if !ok {
				return fmt.Errorf("no mappoings for %s group", s.RequestedType)
			}

			// loop through all mappings of requested group
			for _, g := range m {
				// check if group has defined limit
				if _, ok := limits[g]; !ok {
					s.Position = groups[g] - limits[g] + 1
					return fmt.Errorf("no limit for %s group", s.RequestedType)
				}

				// checks if classification for group has not reached max limit
				if limits[g] == 0 {
					continue
				}

				// assign group, position, decrease limits for chosen group and call function
				s.AssignedType = g
				s.Position = groups[g] - limits[g] + 1
				limits[g]--
				f(*s)
				called = true
				break
			}

			// call function when position and type is not classified
			if !called {
				f(*s)
			}
		}
	}

	return nil
}

func (c csfr) clear(s *domain.Subscription) {
	s.AssignedType = ""
	s.Position = 0
}

func (c csfr) sortPoints(points []float64) {
	sort.Sort(sort.Reverse(sort.Float64Slice(points)))
}

func newClassifier() Classifier {
	return &csfr{}
}
