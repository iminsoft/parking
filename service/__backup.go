package service


//type (
//Person interface {
//	Name() string            //his personal, uniqe ID
//	Class() string           // parking class that he want to participate (pay, free, any)
//	EmploymentAt() time.Time // gives employment maturity - one of parking subscription factors
//	Points() float64         // Currently collected points
//}
//
//// one active per person
//Subscription interface {
//	Person() string  // unique name of person
//	Parking() string // unique name of parking that person subscribes
//	Period() (from, to time.Time)
//	Class() string // parking class, that person applies
//	CreatedAt() time.Time
//	Active() bool // false - no further operation (cancel, change...) allowed. it's calculated by process
//}
//
//Parking interface {
//	Name() string
//	AvailableFrom() time.Time
//	MonthsSwitch() int
//	FreeSlots() int
//	PaidSlots() int
//	//Subscriptions() []Subscription
//}

//Period interface {
//	Start() time.Time
//	End() time.Time
//}
//
//PersonService interface {
//	Get(person string) (Person, error)
//
//	// Add user
//	Create(name string, employedAt time.Time) error
//
//	// Take list of subscribed users
//	//FindPeopleWithActiveSubscription
//	Subscribed(parking string) ([]Person, error)
//
//	// Save person points
//	// hidden?
//	//UpdatePoints(person string, points float64) error
//}
//
//ParkingService interface {
//	// Save parking in order to let person create an subscription
//	Create(Parking) error
//
//	// All active parking
//	All() []Parking
//
//	// Just a parking instance no matter if it's available or not.
//	Get(parking string) (Parking, error)
//
//	// Takes period open for subscriptions, this will check if parking is still available
//	Period(parking string) (from, to time.Time, err error)
//
//	// Defines another period for a parking, thanks to that users can subscribe for it.
//	// hidden?
//	CreatePeriod(parking string, from, to time.Time) error
//
//	// lista z person, points, assignment
//	Ranking(parking string) (*Ranking, error)
//}

// Manage subscriptions, then Manager name?
//SubscriptionService interface {
//	// When subscription is done for part of the period.
//	//PartialCreate(employee, stype string, from, to time.Time) (uint, error)
//	//Context() (Parking, Period)
//
//	// Subscribe person for given parking on given type.
//	// Only one subscription can be create across all parkings
//	Create(person, parking, class string) error
//
//	// Check if subscription is created and return it
//	// FindActiveSubscription
//	Status(person string) (Subscription, error)
//
//	// Remove subscription
//	// DeleteActiveSubscription
//	Cancel(person string) error
//
//	// Take all subscriptions that person participates in past
//	// FindProcessedSubscriptions
//	History(person string) []Subscription
//}

//
//Classifier interface {
//	// Allocate each person to appropriate class. It's performed based on class, points of each person and defined
//	// parking attributes.
//	Classify(Parking, []Person)
//
//	// For defined person and personal history of his subscriptions, calculation determines 3 factors.
//	calculate(Person, []Subscription) (seniority, free, pay float64)
//}

////when is located in memory, will tricky switch curent period with parking.
//Server interface {
//	//Classifier(Parking, Period) Classifier
//	Subscription() SubscriptionService
//	Person() PersonService
//	Parking() ParkingService
//	Classify(Person) error
//}

//SubscriptionCollection interface {
//	Get(employee string)
//	Each(func(Subscription))
//}
//)

//var (
//	defaultRepository Repository = newSQLiteRepository()
//	DefaultContainer  Container  = newContainer(defaultRepository)
//)
