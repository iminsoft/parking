package service

import (
	"fmt"
	"time"

	"bitbucket.org/iminsoft/parking/service/domain"
)

type (
	subscriptionsManager struct {
		calculator Calculator
		classifier Classifier
	}
)

func (m *subscriptionsManager) Subscribe(u domain.User, p *domain.Parking) error {
	if p.Period.Closed {
		return fmt.Errorf("parking %s has closed period, unsubscription is not possible", p.ID)
	}

	pg := p.Groups()
	if _, ok := pg[u.Type]; !ok {
		return fmt.Errorf("Wrong subscription type: %s, allowed options: %v", u.Type, pg)
	}

	for _, s := range p.Period.Subscriptions {
		if s.UserID == u.ID {
			return fmt.Errorf("user %s is already subscribed for %s parking", u.ID, p.ID)
		}
	}

	ds := domain.Subscription{
		User:      u,
		ParkingID: p.ID,
		PeriodID:  p.Period.ID,
		UserID:    u.ID,
		//Period:        p.Period,
		From:          p.Period.From,
		To:            p.Period.To,
		SubscribedAt:  time.Now(),
		Points:        m.calculator.Calculate(u),
		RequestedType: u.Type,
	}

	p.Period.Subscriptions = append(p.Period.Subscriptions, ds)

	return m.classifier.Classify(p, func(domain.Subscription) {})
	//trigger(OnSubscribed, *sbr.Parking().Period)
	//
}

func (m *subscriptionsManager) Unsubscribe(u domain.User, pc map[string]*domain.Parking) error {

	for i := range pc {
		p := pc[i]
		if p.Period.ID == 0 {
			return fmt.Errorf("parking %s has no period", p.ID)
		}

		tmp := p.Period.Subscriptions
		for k, s := range tmp {
			if s.UserID != u.ID {
				continue
			}

			if p.Period.Closed {
				return fmt.Errorf("parking %s has closed period, unsubscription is not possible", p.ID)
			}

			p.Period.Subscriptions = append(tmp[:k], tmp[k+1:]...)

			return m.classifier.Classify(p, func(domain.Subscription) {})
			//	trigger(OnUnsubscribed, *sbr.Parking().Period)
		}
	}

	return fmt.Errorf("user %s is not subscribed", u.ID)
}

func newSubscriptionManager(cr Calculator, cl Classifier) SubscriptionsManager {
	return &subscriptionsManager{cr, cl}
}
