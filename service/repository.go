package service

import (
	"time"

	"bitbucket.org/iminsoft/parking/service/db"
	"bitbucket.org/iminsoft/parking/service/domain"
)

type (
	parkingRepository struct{}
)

func (r *parkingRepository) PersonWithScore(username string) (domain.User, error) {
	return db.FindPersonWithScore(username)
}

func (r *parkingRepository) Person(username string) (domain.User, error) {
	return db.FindPerson(username)
}

func (r *parkingRepository) AllParking() []*domain.Parking {
	pl, _ := db.LoadAllParking(time.Now())
	return pl
}

func (r *parkingRepository) StoreParking(p *domain.Parking) error {
	return db.StoreParking(p)
}

func newRepository() Repository {
	return &parkingRepository{}
}
