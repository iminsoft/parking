package service

import (
	"log"
	"math"
	"time"

	"bitbucket.org/iminsoft/parking/service/domain"
)

var (
	fw = 2.5
	pw = 1.6
	sw = 0.86
)

type (
	calculator struct {
		formula Formula
	}
)

func defaultFormula(seniority, free, pay float64) float64 {
	return (seniority*math.Log10(seniority)+1)*sw - (free * fw) - (pay * pw)
}

func (r *calculator) Calculate(s domain.User) float64 {
	var seniority, free, paid float64

	if len(s.Subscriptions) == 0 {
		log.Printf("user %s has no history to calulate from", s.ID)
	}

	seniority = time.Now().Sub(s.HiredAt).Hours() / 24
	for _, s := range s.Subscriptions {
		days := s.To.Sub(s.From).Hours() / 24
		switch s.AssignedType {
		case "free":
			free += days
			break
		case "paid":
			paid += days
		}
	}

	return r.formula(seniority, free, paid)
}

func newCalculator(f Formula) Calculator {
	return &calculator{f}
}
