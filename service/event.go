package service

import "bitbucket.org/iminsoft/parking/service/domain"

const (
	OnInit         Event = "on.init"
	OnPeriodOpen   Event = "on.period.start"
	OnPeriodClosed Event = "on.period.finished"
	OnFinished     Event = "on.finished"
)

type (
	eventManager struct {
		actions map[Event][]EventFunc
	}

	Event     string
	EventFunc func(*domain.Parking)
)

func newEventManager() eventManager {
	return eventManager{
		actions: make(map[Event][]EventFunc),
	}

}

func (o *eventManager) on(e Event, f EventFunc) {
	o.actions[e] = append(o.actions[e], f)
}

func (o *eventManager) fire(e Event, p *domain.Parking) {
	as, ok := o.actions[e]
	if !ok {
		return
	}

	for _, a := range as {
		a(p)
	}
}


