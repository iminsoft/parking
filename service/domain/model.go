package domain

import (
	"math"
	"time"
)

type (
	Parking struct {
		ID             string    `gorm:"primary_key" json:"id" description:"Name, location of parking"`
		FreePlaces     int       `json:"free_places" description:"Number of paid parking spaces to share between employees "`
		PaidPlaces     int       `json:"paid_places" description:"Number of free parking spaces to share between employees "`
		Price          float32   `json:"price" description:"Monthly cost of paid parking space"`
		Mail           string    `json:"mail" description:"Person responsible for parking"`
		LeaseFrom      time.Time `json:"lease_from" description:"Is date from what parking is available for share"`
		ScheduleMonths int       `json:"schedule_months" description:"Subscription period is open every ScheduleMonths, starts from LeaseFrom"`
		Period         Period
	}

	//It's a time boundary where employees are able to subscribe
	Period struct {
		ID            uint           `gorm:"primary_key" json:"id"`
		ParkingID     string         `json:"-"`
		From          time.Time      `json:"from"`
		To            time.Time      `json:"to"`
		Closed        bool           `json:"closed"`
		Subscriptions []Subscription `gorm:"-"`
	}

	User struct {
		ID            string    `gorm:"primary_key" json:"gid"`
		HiredAt       time.Time `json:"hired_at"` // v
		Type          string    // v
		Result        float64
		Subscriptions []Subscription `gorm:"-"`
	}

	Subscription struct {
		ID        uint
		UserID    string
		PeriodID  uint
		ParkingID string

		User User
		//Period Period

		From          time.Time // v
		To            time.Time // v
		Points        float64   // v
		RequestedType string    // v
		AssignedType  string    // v
		Position      uint      // v
		Processed     bool      // v
		SubscribedAt  time.Time // v
		ProcessedAt   time.Time // v
	}
)

// Person implementation
func (P Parking) Name() string {
	return P.ID
}

func (p Parking) AvailableFrom() time.Time {

	return p.LeaseFrom
}

func (p Parking) MonthsSwitch() int {
	return p.ScheduleMonths
}

func (p Parking) FreeSlots() int {
	return p.FreePlaces
}

func (p Parking) PaidSlots() int {
	return p.PaidPlaces
}

func (p Parking) Groups() map[string]uint {
	return map[string]uint{
		"free": uint(p.FreePlaces),
		"paid": uint(p.PaidPlaces),
		"any":  math.MaxUint32,
	}
}

func (p Parking) Order() map[string][]string {
	return map[string][]string{
		"free": {"free"},
		"paid": {"paid"},
		"any":  {"free", "paid"},
	}
}

func (s Subscription) Group() string {
	return s.RequestedType
}

func (s Subscription) Score() float64 {
	return s.Points
}
