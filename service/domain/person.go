package domain

import "time"

// Person implementation
func (e User) Name() string {
	return e.ID
}

func (e User) EmploymentAt() time.Time {
	return e.HiredAt
}

func (e User) Points() float64 {
	return e.Result
}
