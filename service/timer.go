package service

import (
	"log"
	"time"

	"bitbucket.org/iminsoft/parking/service/domain"
)

type (
	group struct {
		parking   *domain.Parking
		generator Generator
	}

	timer struct {
		done   bool
		events eventManager
	}
)

func (t *timer) Maintain(p *domain.Parking, g Generator) error {
	var err error
	log.Printf("Parking.Timer received new maintened %s parking", p.ID)
	if p.Period.ID == 0 {
		log.Printf("Parking.Timer parking %s has no period", p.ID)
		p.Period, err = g.Generate(*p)
		if err != nil {
			return err
		}
		log.Printf("Parking.Timer period <%s : %s> generated for parking %s",
			p.Period.From.Format("2006-01-02"),
			p.Period.To.Format("2006-01-02"),
			p.ID)

	}
	t.events.fire(OnInit, p)

	go t.process(p, g)

	return nil
}

func (t *timer) Abandon(p domain.Parking) error {
	log.Printf("Parking.Timer removed maintened %s parking", p.ID)
	t.done = true
	return nil
}

func (t *timer) process(parking *domain.Parking, generator Generator) {
	tr := time.NewTicker(time.Second * 5)
	for now := range tr.C {
		if t.done {
			t.events.fire(OnFinished, parking)
			log.Printf("Parking.Timer: finished %s parking", parking.ID)
			return
		}
		// change current date
		if hackedDate.After(now) {
			now = hackedDate
		}

		// check if there are some period to close
		if !now.After(parking.Period.From) {
			continue
		}

		log.Printf("Parking.Timer: closing %s period <%s - %s>\n",
			parking.ID,
			parking.Period.From.Format("2006-01-02"),
			parking.Period.To.Format("2006-01-02"))

		parking.Period.Closed = true
		//Notify about closing period
		t.events.fire(OnPeriodClosed, parking)

		// Create next period for parking
		pd, err := generator.Generate(*parking)
		if err != nil {
			panic(err)
		}

		parking.Period = pd

		log.Printf("Parking.Timer: opening %s period <%s - %s>\n",
			parking.ID,
			parking.Period.From.Format("2006-01-02"),
			parking.Period.To.Format("2006-01-02"))

		t.events.fire(OnPeriodOpen, parking)

	}
}

func newTimer(em eventManager) Timer {
	return &timer{
		done:   false,
		events: em,
	}
}

func SetDate(t time.Time) {
	hackedDate = t
	log.Printf("Period.Timer: hacked, new date %s passed\n", hackedDate.Format("2006-01-02"))

}

func Date() time.Time {
	return hackedDate
}
