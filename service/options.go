package service

type (
	Option func(*Service)
)

func Listen(e Event, f EventFunc) Option {
	return func(s *Service) {
		s.events.on(e, f)
	}
}
