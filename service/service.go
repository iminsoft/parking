package service

import (
	"time"

	"fmt"

	"bitbucket.org/iminsoft/parking/service/domain"
)

var (
	hackedDate time.Time
)

type (
	Repository interface {
		Person(name string) (domain.User, error)
		PersonWithScore(username string) (domain.User, error)
		AllParking() []*domain.Parking
		StoreParking(*domain.Parking) error
	}

	Timer interface {
		Maintain(*domain.Parking, Generator) error
		Abandon(domain.Parking) error
	}

	// Generator generates domain.Period based on domain.Parking data
	Generator interface {
		//Generate creates Period based on given Parking
		Generate(domain.Parking) (domain.Period, error)
	}

	// Calculate based on historical slice of User.Score, Calculator is used to determine number of subscriber points.
	Calculator interface {
		// Calculate takes all aggregated Scores (historical results of subscription) and calculate its results.
		Calculate(domain.User) float64
	}

	Formula func(seniority, free, pay float64) float64

	//Classifier determines
	Classifier interface {
		//Classify
		Classify(*domain.Parking, func(domain.Subscription)) error
	}

	Subscriber interface {
		Subscribe(domain.User, *domain.Parking) error
	}

	Unsubscriber interface {
		Unsubscribe(domain.User, map[string]*domain.Parking) error
	}

	SubscriptionsManager interface {
		Subscriber
		Unsubscriber
	}

	Service struct {
		parking      map[string]*domain.Parking
		repo         Repository
		timer        Timer
		generator    Generator
		subscription SubscriptionsManager
		events       eventManager
	}
)

func (s *Service) HackDate(t time.Time) {
	hackedDate = t
}

func (s *Service) Subscribe(username, name, typ string) error {
	u, err := s.repo.PersonWithScore(username)
	if err != nil {
		return fmt.Errorf("can not subscribe %s user, user %s not exists", username, username)
	}

	u.Type = typ

	p, err := s.get(name)
	if err != nil {
		return err
	}

	return s.subscription.Subscribe(u, p)
}

func (s *Service) Unsubscribe(username string) error {

	u, err := s.repo.Person(username)
	if err != nil {
		return err
	}

	return s.subscription.Unsubscribe(u, s.parking)
}

func (s *Service) Score(username string) (domain.Subscription, error) {
	for _, p := range s.parking {
		for _, s := range p.Period.Subscriptions {
			if s.UserID == username {
				return s, nil
			}
		}
	}

	return domain.Subscription{}, fmt.Errorf("user %s is not subscribed in a parking", username)
}

func (s *Service) Ranking(parking string) ([]domain.Subscription, error) {

	p, err := s.get(parking)
	if err != nil {
		return []domain.Subscription{}, err
	}

	return p.Period.Subscriptions, nil
}

func (s *Service) User(username string) (domain.User, error) {

	return s.repo.PersonWithScore(username)
}

func (s *Service) List() []domain.Parking {
	var ps []domain.Parking
	for _, p := range s.parking {
		ps = append(ps, *p)
	}

	return ps
}

func (s *Service) loadCurrentPeriod(p *domain.Parking) {
	//if err := s.repo.LoadPeriod(p.Period); err != nil {
	//	log.Print(err.Error())
	//	return
	//}
}

func (s *Service) storePeriod(p *domain.Parking) {
	s.repo.StoreParking(p)
}

func (s *Service) get(parking string) (*domain.Parking, error) {
	var p *domain.Parking
	if p, ok := s.parking[parking]; ok {
		return p, nil
	}

	return p, fmt.Errorf("parking %s is not exists", parking)
}

func NewService(opts ...Option) Service {
	em := newEventManager()
	cr := newCalculator(defaultFormula)

	s := Service{
		parking:      make(map[string]*domain.Parking),
		repo:         newRepository(),
		events:       em,
		timer:        newTimer(em),
		generator:    newGenerator(),
		subscription: newSubscriptionManager(cr, newClassifier()),
	}

	// setup default listeners
	//s.events.on(OnInit, s.loadCurrentPeriod)
	//s.events.on(OnPeriodOpen, s.loadCurrentPeriod)
	s.events.on(OnPeriodClosed, s.storePeriod)

	for _, o := range opts {
		o(&s)
	}

	// read all parkings from database in order to setup a service
	for _, p := range s.repo.AllParking() {
		s.parking[p.ID] = p
		s.timer.Maintain(p, s.generator)
	}

	return s
}

//func init() {
//	rt, _ := time.Parse("2006-01-02", "2015-01-01")
//	at, _ := time.Parse("2006-01-02", "2016-05-01")
//	db.AddParking(&domain.Parking{
//		ID:             "Renoma",
//		FreePlaces:     7,
//		PaidPlaces:     8,
//		ScheduleMonths: 3,
//		LeaseFrom:      rt,
//	})
//	db.AddParking(&domain.Parking{
//		ID:             "Arkady",
//		FreePlaces:     3,
//		PaidPlaces:     2,
//		ScheduleMonths: 2,
//		LeaseFrom:      at,
//	})
//
//	timer := period.NewTimer(period.DefaultGenerator).
//		//load current period and it's subscribers
//		Listen(period.OnInit, onPeriodStart).
//		Listen(period.OnOpen, onPeriodStart).
//		Listen(period.OnClose, onPeriodSwitch)
//
//	for _, p := range db.FindAllParking() {
//		timer.Maintain(p)
//	}
//
//}
