package service

import (
	"fmt"
	"time"

	"bitbucket.org/iminsoft/parking/service/domain"
)

type (
	generator struct{}
	asd       time.Time
)

func (g generator) Generate(dg domain.Parking) (domain.Period, error) {
	var dpd domain.Period
	var from time.Time

	now := time.Now()

	if hackedDate.After(now) {
		now = hackedDate
	}

	if dg.MonthsSwitch() == 0 {
		return dpd, fmt.Errorf("Please set month switch for Parking %s", dg.Name())
	}

	begin := dg.AvailableFrom().UTC()
	from = begin
	for {
		from = begin.AddDate(0, dg.MonthsSwitch(), 0)
		if from.After(now) {
			break
		}
		begin = from
	}

	return domain.Period{
		To:        from.AddDate(0, dg.MonthsSwitch(), -1),
		From:      from,
		ParkingID: dg.ID,
		Closed:    false,
	}, nil
}

func newGenerator() Generator {
	return &generator{}
}
